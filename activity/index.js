
// ----------------------------------------------------------------------
// 3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos",{
	method: "GET",
	headers: {
		"Content-Type":"application/json"
	},
})
.then(response => response.json())
.then(json => console.log(json));
// ----------------------------------------------------------------------
// 4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.
fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(json => {
   json.map((element) => {
    console.log(element.title)
  })
})

// ----------------------------------------------------------------------
// 5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "GET",
	headers: {
		"Content-Type":"application/json"
	},
})
.then(response => response.json())
.then(json => console.log(json));
// ----------------------------------------------------------------------
// 6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(response => response.json())
.then(json => 
    console.log(`Title: ${json.title} - Status: Completed ${json.completed}`)
    )
// ----------------------------------------------------------------------
// 7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "POST - New To do list",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json));
// ----------------------------------------------------------------------
// 8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
fetch( "https://jsonplaceholder.typicode.com/todos/1", {
		method: "PUT",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({
			id: 1,
			title:"PUT - Updated to do list",
			userId: 1
		})
	} )
	.then(response => response.json())
	.then(json => console.log(json));
// ----------------------------------------------------------------------
// 9. Update a to do list item by changing the data structure to contain the following properties:
/*- Title
- Description
- Status
- Date Completed
- User ID*/
fetch( "https://jsonplaceholder.typicode.com/todos/1", {
		method: "PUT",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({
			id: 1,
			title:"PUT - Updated to do list",
			status:"Approved",
			dateCompleted:"July 21, 2022",
			userId: 2
		})
	} )
	.then(response => response.json())
	.then(json => console.log(json));
// ----------------------------------------------------------------------
//10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.

fetch( "https://jsonplaceholder.typicode.com/todos/1", {
		method: "PATCH",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({
			title:"PATCH - Corrected to do list"
		})
	} )
	.then(response => response.json())
	.then(json => console.log(json));
// ----------------------------------------------------------------------
// 11. Update a to do list item by changing the status to complete and add a date when the status was changed.
fetch( "https://jsonplaceholder.typicode.com/todos/1", {
		method: "PATCH",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({
			status:"PATCH - Complete",
			dateCompleted: "July 21, 2022"
		})
	} )
	.then(response => response.json())
	.then(json => console.log(json));
// ----------------------------------------------------------------------
// 12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
fetch( "https://jsonplaceholder.typicode.com/todos/1", {
		method: "DELETE"
	} )
.then(response => response.json())
.then(json => console.log(json));